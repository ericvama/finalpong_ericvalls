/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/
#include "raylib.h"


typedef enum GameScreen { LOGO, TITLE, OPTIONS, GAMEPLAY, CREDITS, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    char windowTitle[30] = "raylib game - FINAL PONG";
    int screenWidth = 800;
    int screenHeight = 450;
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    
    bool bugmode = true;
    float seconds;
    Vector2 mousePoint = { 0.0f, 0.0f };
    
    //------------------------------------------------------------------------
    
    // Inicializar cosas Logo
    float framesCounter = 0;
    int lettersCount = 0;   // animacion aparecer letras logo
    int state = 0;          // 0 = fade in lineas rectangulo, 1 = aparicion letras, 2 = esperar 2 segundos, 3 = fade out lineas rectangulo, 4 = fin logo
    float alpha = 0;        // opacidad para poder hacer fade
    
    //------------------------------------------------------------------------
    
    // Inicializar cosas Title
    bool animaciontitulo = true;
    int velocidadtitulo = 6;
    Vector2 postitle;
        postitle.x = -300;
        postitle.y = 50;
    char * titulo = "FINAL PONG";
    Vector2 longitudtitulo;         // Lo declaro despues del init window
    int tamanotitulo = 80;
    
    Rectangle underline;
        underline.x = 0;
        underline.y = 130;
        underline.width = 0;
        underline.height = 12;
    
    char * subtitulo;
    int tamanosubtitulo = 26;
    int framesCounterTitle = 0;     // Contador para el blink de press enter
    bool blink = true;
    
    //BOTONES
    // Posicion de la sprite dentro del spritesheet
    Rectangle sourceBoton = { 0, 94, 190, 49 };
    Rectangle sourceBotonpressed = { 190, 49, 190, 45 };
    
    // Posicion del boton en la pantalla y tamano
    Rectangle posBotonOptions;
        posBotonOptions.width = sourceBoton.width - 40;        //Para poder escalar la sprite
        posBotonOptions.height = sourceBoton.height;
        posBotonOptions.x = screenWidth/2 - posBotonOptions.width/2;
        posBotonOptions.y = screenHeight/2 + 70;
       
    Rectangle posBotonpressedOptions;
        posBotonpressedOptions.width = sourceBotonpressed.width - 40;
        posBotonpressedOptions.height = sourceBotonpressed.height;
        posBotonpressedOptions.x = posBotonOptions.x;
        posBotonpressedOptions.y = posBotonOptions.y + 4;
        
    Rectangle posBotonCredits;
        posBotonCredits.width = posBotonOptions.width;        //Para poder escalar la sprite
        posBotonCredits.height = posBotonOptions.height;
        posBotonCredits.x = posBotonOptions.x;
        posBotonCredits.y = screenHeight/2 + 130;
        
    Rectangle posBotonpressedCredits;
        posBotonpressedCredits.width = posBotonpressedOptions.width;
        posBotonpressedCredits.height = posBotonpressedOptions.height;
        posBotonpressedCredits.x = posBotonCredits.x;
        posBotonpressedCredits.y = posBotonCredits.y + 4;
        
        
        
    // Estado de los botones
    int StateBotonOptions = 0;               // Button state: 0-NORMAL, 1-MOUSE_HOVER, 2-PRESSED
    int StateBotonCredits = 0;               // Button state: 0-NORMAL, 1-MOUSE_HOVER, 2-PRESSED
    
    Vector2 origin;
        origin.x = 0;
        origin.y = 0;
    
    //------------------------------------------------------------------------
    // Inicializar cosas Options
    Rectangle underlineoptions;
        underlineoptions.x = 0;
        underlineoptions.y = 100;
        underlineoptions.width = 0;
        underlineoptions.height = 10;
        
    bool singleplayer = true;
    bool darkbackground = false;
    Color backgroundcolor;
    Color textcolor = BLACK;
    int difficultycounter = 1;
    int maxgoals = 3;
    
    
    //BOTONES
    // Posicion de la sprite dentro del spritesheet
    Rectangle sourceSliderleft = { 339, 143, 39, 31};
    Rectangle sourceSliderright = { 378, 143, 39, 31};
    Rectangle sourceWhitebox = { 0, 0, 195, 49};
    Rectangle sourceCheck = { 0, 0, 38, 36};
    
    //Rectangle sourceSliderright = { 378, 143, 39, 31};
    //Rectangle sourceWhitebox = { 0, 0, 195, 49};
    
    // Posicion del boton en la pantalla y tamano
    Rectangle posBotonSliderleft;
        posBotonSliderleft.x = screenWidth/2 - 40;
        posBotonSliderleft.y = 154;
        posBotonSliderleft.width = sourceSliderleft.width/1.5;
        posBotonSliderleft.height = sourceSliderleft.height/1.5;
    
    Rectangle posBotonSliderright;
        posBotonSliderright.x = screenWidth/2 + 170;
        posBotonSliderright.y = posBotonSliderleft.y;
        posBotonSliderright.width = sourceSliderright.width/1.5;
        posBotonSliderright.height = sourceSliderright.height/1.5;
    
    Rectangle posBotonWhitebox;
        posBotonWhitebox.x = posBotonSliderleft.x + 33;
        posBotonWhitebox.y = posBotonSliderleft.y - 6;
        posBotonWhitebox.width = 170;
        posBotonWhitebox.height = 31;
        
    Rectangle posBotonSliderleft1;
        posBotonSliderleft1.x = posBotonSliderleft.x;
        posBotonSliderleft1.y = posBotonSliderleft.y + 50;
        posBotonSliderleft1.width = posBotonSliderleft.width;
        posBotonSliderleft1.height = posBotonSliderleft.height;
    
    Rectangle posBotonSliderright1;
        posBotonSliderright1.x = screenWidth/2 + 170;
        posBotonSliderright1.y = posBotonSliderleft1.y;
        posBotonSliderright1.width = posBotonSliderright.width;
        posBotonSliderright1.height = posBotonSliderright.height;
        
    Rectangle posBotonWhitebox1;
        posBotonWhitebox1.x = posBotonWhitebox.x;
        posBotonWhitebox1.y = posBotonSliderleft1.y - 6;
        posBotonWhitebox1.width = posBotonWhitebox.width;
        posBotonWhitebox1.height = posBotonWhitebox.height;
        
    
    
    
    
    Rectangle posBotonCheck;
        posBotonCheck.x = posBotonSliderleft.x + 75;
        posBotonCheck.y = posBotonWhitebox1.y + 50;
        posBotonCheck.width = sourceCheck.width/1.5;
        posBotonCheck.height = sourceCheck.height/1.5;
        
    
    
    
    
    
    int StateBotonBacktomenu = 0;               // Button state: 0-NORMAL, 1-MOUSE_HOVER, 2-PRESSED
    //------------------------------------------------------------------------
    
    // Inicializar cosas Gameplay
    bool pausescore = true; //de moment fals per comprovar
    bool pause = false;
    int playerLife = maxgoals;
    int enemyLife = maxgoals;
    
    int secondsCounter = 99;
    float tamanocontador;
    int iaLinex = screenWidth/2; //Modo normal de serie, se podra combiar en opciones
    int balldirection;
    
    // Inicializar "textura" del campo
    Color colorCampo = GREEN;
    
        //Inicializar rectangulo techo y suelo
    Rectangle techo;
    techo.x = 0;
    techo.y = 0;
    techo.width = screenWidth;
    techo.height = 40;
    
    Rectangle suelo;
    suelo.width = screenWidth;
    suelo.height = 20;
    suelo.x = 0;
    suelo.y = screenHeight - suelo.height;
    
    Rectangle discontinuas;      //Rectangulo para hacer discontinuas
        discontinuas.width = 10;
        discontinuas.height = 25;
        discontinuas.x = screenWidth/2 - discontinuas.width/2;
        discontinuas.y = 0;
    
    Vector2 poscirculo;
        poscirculo.x = screenWidth/2;
        poscirculo.y = screenHeight/2;
        
    
    
    //Inicializar rectangulo jugadores y sus correspondientes velocidades
    Rectangle player;
    player.width = 15;
    player.height = 80;
    player.x = 20;
    player.y = screenHeight/2 - player.height/2;
    int playerSpeedY = 6;
    
    Rectangle enemy;
    enemy.width = 15;
    enemy.height = 80;
    enemy.x = screenWidth - enemy.width - 20;
    enemy.y = screenHeight/2 - enemy.height/2;
    int enemySpeedY = 6;
    
    //Inicializar pelota (posicion, velocidad y tamano)
    Vector2 ballPosition;
    ballPosition.x = screenWidth/2;
    ballPosition.y = screenHeight/2;
    
    int ballSpeedini = 4;
    int maxvelball = 20;
    
    Vector2 ballSpeed;
    ballSpeed.x = ballSpeedini;
    ballSpeed.y = ballSpeedini;
    
    int ballRadius = 10;
    
    //Inicializar contador pelota    
    int collisionballplayer = 0;
    
    Rectangle backRect1 = { 20, 8, 100, 25}; //Background Rectangle
    Rectangle backRect2 = { screenWidth - backRect1.width - 20, 8, 100, 25}; //Background Rectangle
    Rectangle fillRect1 = { backRect1.x + 2, backRect1.y + 2, backRect1.width - 4, backRect1.height - 4};
    Rectangle fillRect2 = { backRect2.x + 2, backRect2.y + 2, backRect2.width - 4, backRect2.height - 4};
    Rectangle lifeRect1 = fillRect1;
    Rectangle lifeRect2 = fillRect2;
    //lifeRect.width /= 2;
    
    
    
        
    //int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    
    //------------------------------------------------------------------------
    
    // Inicializar cosas Ending
    char * result;
    char * result2linea;
    Color colorresultado;
    
    
    
    
    
    
    
    
    
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    Texture2D bluesheet = LoadTexture("resources/blueSheet.png"); // Load button texture
    Texture2D whitebox = LoadTexture("resources/grey_button05.png");
    Texture2D checkmark = LoadTexture("resources/green_boxCheckmark.png");
    Texture2D cross = LoadTexture("resources/red_boxCross.png");
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    Font tituloTtf = LoadFontEx ("Resources/Gameplay.ttf", tamanotitulo, 0, 250);
        longitudtitulo = MeasureTextEx(tituloTtf, titulo, tamanotitulo, 5);
        underline.x = screenWidth/2 - longitudtitulo.x/2;
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    InitAudioDevice();
    Sound choque = LoadSound("Resources/bounce.wav");
    Sound incrementovel = LoadSound("Resources/masvelocidad.wav");
    Sound goal = LoadSound("Resources/score.wav");
    Sound pausar = LoadSound("Resources/pause.wav");
    Sound continuar = LoadSound("Resources/continue.wav");
    Sound ganar = LoadSound("Resources/win.wav");
    Sound boton = LoadSound("Resources/button.ogg");   // Load button sound
    
    Music music = LoadMusicStream("resources/musicafondo.ogg");
    PlayMusicStream (music);
    float musicvol = 0.1f;
    float soundvol = 0.1f;
    
    
    
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        
        if (IsKeyPressed (KEY_B))
        {
            bugmode = !bugmode;
        }
        
        seconds = framesCounter/60;
        
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                
                if (state == 0)                  // State 0 = fade in lineas rectangulo
                {
                    framesCounter++;
                    
                    alpha += 1.0f/90;                // Fade in dura 1,5 segundos
                    
                    if (alpha >= 1.0f)               // Cuando acabe fade in cambiar a state 1
                    {
                        state = 1;
                        framesCounter = 0.0f;               // Reset counter
                    }
                }
                
                else if (state == 1)             // State 1 = aparicion letras
                {
                    framesCounter++;
                    
                    if (framesCounter == 12)            // Cada 12 frames, aparece otra letra
                    {
                        lettersCount++;
                        framesCounter = 0.0f;
                    }

                    if (lettersCount >= 10)          // Cuado hayan aparecido todas las letras pasar a state 2
                    {
                        state = 2;
                    }
                }
                
                else if (state == 2)             // State 2 = esperar 2 segundos
                {
                    framesCounter++;
                    
                    if (framesCounter == 120)        // Cuando hayan pasado 2 sec cambiar a state 3
                    {
                        state = 3;
                        framesCounter = 0;
                    }
                }
                
                else if (state == 3)             // State 3 = fade out lineas rectangulo
                {
                    framesCounter++;
                    
                    alpha -= 1.0f/90;                // Fade out dura 1,5 segundos

                    if (alpha <= 0.0f)               // Cuando acabe fade out cambiar a screen TITLE
                    {
                        alpha = 0.0f;
                        framesCounter = 0;
                        state = 0;
                        screen = TITLE;
                    }
                }
                
                
            } break;
            
            
            //----------------------------------------------------------------------------------
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                
                
                // TODO: Title animation logic.......................(0.5p)
                
                if (animaciontitulo)
                {
                    framesCounter++;
                    if (postitle.x < (screenWidth/2 - longitudtitulo.x/2)){
                        if (postitle.x > 100)
                        {
                            postitle.x = postitle.x + velocidadtitulo - framesCounter/30;
                        }
                        else
                        {
                            postitle.x += velocidadtitulo;
                        }
                    }
                    else if (state == 0) // Cuando el titulo este centrado...
                    {
                        framesCounter = 0;
                        state = 1;
                    }
                    else if (state == 1)        //Aparece subrayado
                    {
                        if (framesCounter >= 30)                           // Esperar medio
                        {
                            if (underline.width < longitudtitulo.x)        // Dibujar linea
                            {
                                underline.width += velocidadtitulo;
                            }
                            else
                            {
                                framesCounter = 0;
                                state = 2;
                            }
                        }
                    }
                    else if (state == 2)        //Aparece made by eric
                    {
                        if (framesCounter >= 30)                           // Esperar medio
                        {
                            if (alpha < 1.0f)               // Cuando acabe fade in cambiar a screen TITLE
                            {
                                alpha += 1.0f/60;           // Fade in dura 1 segundos
                            }
                            else
                            {
                                framesCounter = 0;
                                state = 3;
                            }
                        }
                    }
                    else if (state == 3)
                    {
                        if (framesCounter >= 60)                           // Esperar medio
                        {
                            framesCounter = 0;
                            state = 0;
                            animaciontitulo = !animaciontitulo;
                        }
                    }
                }
                
                if (!animaciontitulo)
                {
                    framesCounterTitle++; //Para el funcionamiento del texto parpadeante
                    mousePoint = GetMousePosition();
                    
                    // TODO: "PRESS ENTER" logic.........................(0.5p)
                    if (IsKeyPressed(KEY_ENTER))
                    {
                        if (pause)
                        {
                            pause = !pause;
                        }
                        balldirection = GetRandomValue(0, 1);
                            if (balldirection == 0)
                            {
                                ballSpeed.x = -ballSpeedini;
                            }
                            else if (balldirection == 1)
                            {
                                ballSpeed.x = ballSpeedini;
                            }
                        screen = GAMEPLAY;
                    }
                
                    //Funcionamiento blink
                    if (framesCounterTitle % 40 == 0)
                    {
                        framesCounterTitle = 0;
                        blink = !blink;
                    }
                    
                    
                    //Chequear collision raton y boton de opciones
                    if (CheckCollisionPointRec(mousePoint, posBotonOptions))
                    {
                        if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) StateBotonOptions = 2;
                        else StateBotonOptions = 1;

                        if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)){
                            PlaySound(boton);
                            screen = OPTIONS;
                        }
                    }
                    else StateBotonOptions = 0;
                    
                    //Chequear collision raton y boton de creditos
                    if (CheckCollisionPointRec(mousePoint, posBotonCredits))
                    {
                        if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) StateBotonCredits = 2;
                        else StateBotonCredits = 1;

                        if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)){
                            PlaySound(boton);
                            screen = CREDITS;
                        }
                    }
                    else StateBotonCredits = 0;
                }
                
                
                
            } break;
            
            case OPTIONS:
            {
                mousePoint = GetMousePosition();
                
                if ((CheckCollisionPointRec(mousePoint, posBotonSliderleft)) || (CheckCollisionPointRec(mousePoint, posBotonSliderright)))
                {
                    if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)){
                        singleplayer = !singleplayer;
                        PlaySound(boton);
                    }
                }
                
                if (CheckCollisionPointRec(mousePoint, posBotonSliderleft1))
                {
                    if (difficultycounter >= 0){
                        if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)){
                            difficultycounter -= 1;
                            PlaySound(boton);
                        }
                    }
                    else {
                        difficultycounter = 3;
                    }
                }
                
                if (CheckCollisionPointRec(mousePoint, posBotonSliderright1))
                {
                    if (difficultycounter <= 3){
                        if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)){
                            difficultycounter += 1;
                            PlaySound(boton);
                        }
                    }
                    else {
                        difficultycounter = 0;
                    }
                }
                
                
                if (difficultycounter == 0)
                {
                    enemySpeedY = 3;
                    player.height = 80;
                    maxvelball = 20;
                    iaLinex = screenWidth/1.5;
                }
                if (difficultycounter == 1)
                {
                    enemySpeedY = 6;
                    player.height = 80;
                    maxvelball = 20;
                    iaLinex = screenWidth/2;
                }
                if (difficultycounter == 2)
                {
                    enemySpeedY = 8;
                    player.height = 60;
                    maxvelball = 20;
                    iaLinex = screenWidth/3;
                }
                if (difficultycounter == 3)
                {
                    enemySpeedY = 9;
                    player.height = 50;
                    maxvelball = 40;
                    iaLinex = screenWidth/4;
                }
                
                
                
                
                
                if (CheckCollisionPointRec(mousePoint, posBotonCheck))
                {
                    if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)){
                        PlaySound(boton);
                        darkbackground = !darkbackground;
                    }
                }
                
                
                if (CheckCollisionPointRec(mousePoint, posBotonCredits))
                {
                    if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) StateBotonCredits = 2;
                    else StateBotonCredits = 1;

                    if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)){
                        PlaySound(boton);
                        screen = TITLE;
                    }
                }
                else StateBotonCredits = 0;
                
            }break;
            
            //----------------------------------------------------------------------------------
            case GAMEPLAY:
            { 
                
                    UpdateMusicStream(music);   // Update music buffer with new stream data
                    PlayMusicStream(music);
                    SetMusicVolume (music, musicvol);
                    SetSoundVolume (choque, soundvol);
                    SetSoundVolume (goal, soundvol);
                    SetSoundVolume (pausar, soundvol);
                    SetSoundVolume (continuar, soundvol);
                    SetSoundVolume (ganar, soundvol);
                    SetSoundVolume (incrementovel, soundvol);
                    SetSoundVolume (boton, soundvol);
                
                // Update GAMEPLAY screen data here!
                if (IsKeyReleased (KEY_M))
                {
                    playerLife = maxgoals;
                    enemyLife = maxgoals;
                    pausescore = !pausescore;
                    lifeRect1.width = fillRect1.width;
                    lifeRect2.width = fillRect2.width;
                    //Reiniciar posicion pelota y palas
                    ballPosition.x = screenWidth/2;
                    ballPosition.y = screenHeight/2;
                    player.y = screenHeight/2 - player.height/2;
                    enemy.y = screenHeight/2 - enemy.height/2;
                    StopMusicStream(music);
                    secondsCounter = 99;
                    screen = TITLE;
                }
                    
                //Permitir movimiento jugadores mientras no este en pausa ni (se haya marcado y aun no se haya pulsado Spacebar)
                if ((!pausescore) && (!pause))
                {
                    // TODO: Ball movement logic.........................(0.2p)
                    ballPosition.x += ballSpeed.x;
                    ballPosition.y += ballSpeed.y;
                    
                    // TODO: Player movement logic.......................(0.2p)
                    if (singleplayer)
                    {
                        if (IsKeyDown(KEY_UP))
                        {
                            player.y -= playerSpeedY;
                            if (player.y < techo.height)
                            {
                                player.y = techo.height;
                            }
                        }
                        
                        if (IsKeyDown(KEY_DOWN))
                        {
                            player.y += playerSpeedY;
                            if (player.y > screenHeight - player.height - suelo.height)
                            {
                                player.y = screenHeight - player.height - suelo.height;
                            }
                        }
                        
                        if( ballPosition.x > iaLinex)
                        {
                            if(ballPosition.y > enemy.y + enemy.height/2)
                            {
                                enemy.y += enemySpeedY;
                                if (enemy.y > screenHeight - enemy.height - suelo.height)
                                {
                                    enemy.y = screenHeight - enemy.height - suelo.height;
                                }
                            }
                
                            if(ballPosition.y < enemy.y + enemy.height/2)
                            {
                                enemy.y -= enemySpeedY;
                                if (enemy.y < techo.height)
                                {
                                    enemy.y = techo.height;
                                }
                            }
                        }
                        
                        
                    }
                
                    // TODO: Enemy movement logic (IA)...................(1p)
                    else //Si no esta en modo singleplayer
                    {
                        if (IsKeyDown(KEY_UP))
                        {
                            enemy.y -= enemySpeedY;
                            if (enemy.y < techo.height)
                            {
                                enemy.y = techo.height;
                            }
                        }
                        
                        if (IsKeyDown(KEY_DOWN))
                        {
                            enemy.y += enemySpeedY;
                            if (enemy.y > screenHeight - enemy.height - suelo.height)
                            {
                                enemy.y = screenHeight - enemy.height - suelo.height;
                            }
                        }
                        
                        
                        if (IsKeyDown(KEY_W))
                        {
                            player.y -= playerSpeedY;
                            if (player.y < techo.height)
                                {
                                    player.y = techo.height;
                                }
                        }
                   
                        if (IsKeyDown(KEY_S))
                        {
                            player.y += playerSpeedY;
                            if (player.y > screenHeight - player.height - suelo.height)
                                {
                                    player.y = screenHeight - player.height - suelo.height;
                                }
                        }
                        
                    }
                
                        
                    // TODO: Collision detection (ball-player) logic.....(0.5p)
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, (Rectangle){ player.x, player.y, player.width, player.height})){
                        if (ballSpeed.x < 0){
                            collisionballplayer ++;
                            if ((ballSpeed.x < maxvelball) && (ballSpeed.x > -maxvelball)){
                                if (collisionballplayer % 3 == 0){ //si el contador de toques es divisor de 3 y colisiona pala y pelota, incrementar velocidad y reproducir sonido incrmentovel
                                    ballSpeed.x -= 1.0f;
                                    PlaySound (incrementovel);
                                }
                            }
                            ballSpeed.x *= -1;
                        }
                        //No reproducir sonido choque si contador llega a divisor de 3, ya que se reproducira otro sonido
                        if ((collisionballplayer % 3 != 0) || (ballSpeed.x >= maxvelball)){
                        PlaySound(choque);
                        }
                    }
                    
                    // TODO: Collision detection (ball-enemy) logic......(0.5p)
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, (Rectangle){ enemy.x, enemy.y, enemy.width, enemy.height})){
                        if (ballSpeed.x > 0){
                            collisionballplayer ++;
                            if ((ballSpeed.x < maxvelball) && (ballSpeed.x > -maxvelball)){
                                if (collisionballplayer % 3 == 0){ //si el contador de toques es divisor de 3 y colisiona pala y pelota, incrementar velocidad y reproducir sonido incrmentovel
                                    ballSpeed.x += 1.0f;
                                    PlaySound (incrementovel);
                                }
                            }
                            ballSpeed.x *= -1;
                        }
                        //No reproducir sonido choque si contador llega a divisor de 3, ya que se reproducira otro sonido
                        if ((collisionballplayer % 3 != 0) || (ballSpeed.x <= -maxvelball)){
                        PlaySound(choque);
                        }
                    }
                    
                    // TODO: Collision detection (ball-limits) logic.....(1p)
                    //Colision con techo y suelo
                    if ((ballPosition.y < techo.height + ballRadius) || (ballPosition.y > screenHeight - suelo.height - ballRadius))
                    {
                        ballSpeed.y *= -1;
                        PlaySound(choque);
                    }
                    
                    // TODO: Life bars decrease logic....................(1p)
                    if (ballPosition.x < 0){
                        playerLife -= 1;
                        pausescore = !pausescore;
                        lifeRect1.width -= ((1.f/maxgoals)*fillRect1.width);
                        //Reiniciar posicion pelota y palas
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        player.y = screenHeight/2 - player.height/2;
                        enemy.y = screenHeight/2 - enemy.height/2;
                        //Reiniciar velocidad bola
                        balldirection = GetRandomValue(0, 1);
                        if (balldirection == 0)
                        {
                            ballSpeed.x = -ballSpeedini;
                        }
                        else if (balldirection == 1)
                        {
                            ballSpeed.x = ballSpeedini;
                        }
                    }
                    
                    if (ballPosition.x > screenWidth - ballRadius){
                        enemyLife -= 1;
                        pausescore = !pausescore;
                        lifeRect2.width -= ((1.f/maxgoals)*fillRect2.width);
                        //Reiniciar posicion pelota y palas
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        player.y = screenHeight/2 - player.height/2;
                        enemy.y = screenHeight/2 - enemy.height/2;
                        //Reiniciar velocidad bola
                        balldirection = GetRandomValue(0, 1);
                        if (balldirection == 0)
                        {
                            ballSpeed.x = -ballSpeedini;
                        }
                        else if (balldirection == 1)
                        {
                            ballSpeed.x = ballSpeedini;
                        }
                        ballSpeed.y = ballSpeedini;
                    }

                    // TODO: Time counter logic..........................(0.2p)
                    framesCounter++;
                    if (framesCounter == 60)
                    {
                        secondsCounter--;
                        framesCounter = 0;
                    }
                }
   
                    // TODO: Game ending logic...........................(0.2p)
                    if (playerLife == 0)
                        {
                            if (singleplayer)
                            {
                                result = "OOOH, YOU LOSE.";
                                result2linea = "YOU WILL DO IT BETTER NEXT TIME";
                                colorresultado = RED;
                            }
                            else{
                                result = "PLAYER 2 WIN.";
                                colorresultado = RED;
                            }
                            
                        }
                        
                    if (enemyLife == 0)
                        {
                            if (singleplayer)
                            {
                                result = "WOW, YOU WIN.";
                                result2linea = "YOU HAVE JUST BEATEN THE COMPUTER. AREN'T YOU A HECKER, RIGHT?";
                                colorresultado = GREEN;
                            }
                            else{
                                result = "PLAYER 1 WIN.";
                                colorresultado = GREEN;
                            }
                            
                        }
                    
                    if (secondsCounter == 0)
                        {
                            result = "DRAW GAME.";
                            result2linea = "BOTH OF YOU ARE DIFFICULT TO BEAT";
                            colorresultado = ORANGE;
                        }
                        
                    if ((playerLife == 0) || (enemyLife == 0) || (secondsCounter == 0))
                        {
                            lifeRect1.width = fillRect1.width;
                            lifeRect2.width = fillRect2.width;
                            playerLife = maxgoals;
                            enemyLife = maxgoals;
                            secondsCounter = 99;
                            StopMusicStream(music);
                            PlaySound (ganar);
                            
                            screen = ENDING;        
                        }
                        
                    
                    
                    // TODO: Pause button logic..........................(0.2p)
                    if ((IsKeyReleased(KEY_P)) && (!pausescore))
                    {
                        pause = !pause;
                    }
                    
                    //Empezar partida. Desactivar pausescore y reproducir sonido continuar
                    if ((IsKeyPressed(KEY_SPACE)) && (ballPosition.x == screenWidth/2) && (ballPosition.y == screenHeight/2)){
                        collisionballplayer = 0;
                        pausescore = !pausescore;
                        PlaySound (continuar);
                    }
                    
                    
              //  }
                
            } break;
            
            //----------------------------------------------------------------------------------
            
            case CREDITS:
            {
                mousePoint = GetMousePosition();
                
                if (CheckCollisionPointRec(mousePoint, posBotonCredits))
                {
                    if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) StateBotonCredits = 2;
                    else StateBotonCredits = 1;

                    if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)){
                        PlaySound(boton);
                        screen = TITLE;
                    }
                }
                else StateBotonCredits = 0;
                
            } break;
            
            //----------------------------------------------------------------------------------
            
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                if (IsKeyReleased(KEY_ENTER))
                {
                    screen = TITLE;
                }
                //Para salir hay que pulsar el boton ESC, asi que no es necesario escribir la logica del boton ESC
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        
        
        
        
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            if (darkbackground){
                backgroundcolor = BLACK; //No seria necesario crear esta variable pero la usare mas adelante.
                ClearBackground(backgroundcolor);
                textcolor = WHITE;
            }
            else{
                backgroundcolor = RAYWHITE;
                ClearBackground(backgroundcolor);
                textcolor = BLACK;
            }
            
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    if (state == 0){
                        DrawRectangle(screenWidth/2 - 200, screenHeight/2 - 50, 400, 100, Fade(BLACK, alpha));
                        DrawRectangle(screenWidth/2 - 190, screenHeight/2 - 40, 380, 80, RAYWHITE);
                        //DrawRectangle(screenWidth + 10, screenHeight + 10, 390, 190, RAYWHITE);
                    }
                    
                    else if (state == 1){
                        DrawRectangle(screenWidth/2 - 200, screenHeight/2 - 50, 400, 100, Fade(BLACK, alpha));
                        DrawRectangle(screenWidth/2 - 190, screenHeight/2 - 40, 380, 80, RAYWHITE);
                        //tamanologo = MeasureText ("ERIC VALLS", 50);
                        DrawText(TextSubtext("ERIC VALLS", 0, lettersCount), screenWidth/2 - 160, screenHeight/2 - 20, 50, BLACK);
                    }
                    
                    //DrawText("ERIC VALLS", screenWidth/2 - MeasureText("ERIC VALLS",30)/2, screenHeight/2, 30, Fade(BLACK, alpha));
                    
                    else if (state == 2){
                        DrawRectangle(screenWidth/2 - 200, screenHeight/2 - 50, 400, 100, Fade(BLACK, alpha));
                        DrawRectangle(screenWidth/2 - 190, screenHeight/2 - 40, 380, 80, RAYWHITE);
                        DrawText(TextSubtext("ERIC VALLS", 0, lettersCount), screenWidth/2 - 160, screenHeight/2 - 20, 50, BLACK);
                    }
                    
                    else if (state ==3){
                        DrawRectangle(screenWidth/2 - 200, screenHeight/2 - 50, 400, 100, Fade(BLACK, alpha));
                        DrawRectangle(screenWidth/2 - 190, screenHeight/2 - 40, 380, 80, RAYWHITE);
                        DrawText(TextSubtext("ERIC VALLS", 0, lettersCount), screenWidth/2 - 160, screenHeight/2 - 20, 50, Fade(BLACK, alpha));
                    }
                    
                    
                    
                    if (bugmode)
                    {
                        DrawText(FormatText("seconds: %1.1f", seconds), screenWidth - 200, 20, 20, BLACK);
                        DrawText(FormatText("state: %d", state), screenWidth - 200, 40, 20, BLACK);
                        if (state == 0){
                            DrawText("(Fade in)", screenWidth - 108, 43, 15, BLACK);
                        }
                        if (state == 2){
                            DrawText("(Wait 2 sec)", screenWidth - 108, 43, 15, BLACK);
                        }
                        if (state == 3){
                            DrawText("(Fade out)", screenWidth - 108, 43, 15, BLACK);
                        }
                        DrawText(FormatText("alpha: %1.1f", alpha), screenWidth - 200, 60, 20, BLACK);
                    }
                } break;
                
                //----------------------------------------------------------------------------------
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                    DrawTextEx(tituloTtf, titulo, (Vector2){postitle.x, postitle.y}, tamanotitulo, 5, RED);
                    DrawRectangleRec (underline, RED);
                    subtitulo = "Made by Eric Valls";
                    tamanosubtitulo = 26;
                    DrawTextEx(tituloTtf, subtitulo, (Vector2){postitle.x, postitle.y + 100}, tamanosubtitulo - 6, 3, Fade(RED, alpha));
                    
                    
                    
                    if (!animaciontitulo)
                    {
                        // TODO: Draw "PRESS ENTER" message..............(0.2p)
                        subtitulo = "Press ENTER to START";
                        longitudtitulo = MeasureTextEx (tituloTtf, subtitulo, tamanosubtitulo, 3);
                        if (blink)
                        {
                        DrawTextEx(tituloTtf, subtitulo, (Vector2){screenWidth/2 - longitudtitulo.x/2, screenHeight/2 - 10}, tamanosubtitulo, 3, GREEN);
                        }
                        
                        //Controles:
                        DrawText("CONTROLS:", screenWidth - MeasureText("P2-MOVE: ARROWS KEY",tamanotitulo/10) - 20, screenHeight -110, tamanotitulo/4, RED);
                        DrawText("P: PAUSE", screenWidth - MeasureText("P2-MOVE: ARROWS KEY",tamanotitulo/10) - 20, screenHeight - 90, tamanotitulo/9, RED);
                        DrawText("M: MENU", screenWidth - MeasureText("P2-MOVE: ARROWS KEY",tamanotitulo/10) - 20, screenHeight - 75, tamanotitulo/9, RED);
                        DrawText("ESC: EXIT", screenWidth - MeasureText("P2-MOVE: ARROWS KEY",tamanotitulo/10) - 20, screenHeight - 60, tamanotitulo/10, RED);
                        if (singleplayer)
                        {
                            DrawText("MOVE: ARROW KEYS", screenWidth - MeasureText("P2-MOVE: ARROWS KEY",tamanotitulo/10) - 20, screenHeight - 45, tamanotitulo/10, RED);
                        }
                        
                        if (!singleplayer)
                        {
                            DrawText("P1-MOVE: WS", screenWidth - MeasureText("P2-MOVE: ARROWS KEY",tamanotitulo/10) - 20, screenHeight - 45, tamanotitulo/10, RED);
                            DrawText("P2-MOVE: ARROW KEYS", screenWidth - MeasureText("P2-MOVE: ARROWS KEY",tamanotitulo/10) - 20, screenHeight - 30, tamanotitulo/10, RED);
                        }
                        
                        
                        
                        //-----------------------------------------------------
                        if (StateBotonOptions == 0){
                            DrawTexturePro(bluesheet, sourceBoton, posBotonOptions, origin, 0, WHITE);
                            DrawText("OPTIONS", screenWidth/2 - MeasureText("OPTIONS", tamanosubtitulo - 4)/2, posBotonOptions.y + 12, tamanosubtitulo - 4, RED);
                        }
                        if (StateBotonOptions == 1){
                            DrawTexturePro(bluesheet, sourceBoton, posBotonOptions, origin, 0, WHITE);
                            DrawText("OPTIONS", screenWidth/2 - MeasureText("OPTIONS",tamanosubtitulo - 4)/2, posBotonOptions.y + 12, tamanosubtitulo - 4, WHITE);
                        }
                        else if (StateBotonOptions == 2){  //POSAR AIXO EN DRAWING
                            DrawTexturePro(bluesheet, sourceBotonpressed, posBotonpressedOptions, origin, 0, WHITE);
                            DrawText("OPTIONS", screenWidth/2 - MeasureText("OPTIONS",tamanosubtitulo - 4)/2, posBotonpressedOptions.y + 12, tamanosubtitulo - 4, WHITE);
                        }
                        
                        //-----------------------------------------------------
                        if (StateBotonCredits == 0){
                            DrawTexturePro(bluesheet, sourceBoton, posBotonCredits, origin, 0, WHITE);
                            DrawText("CREDITS", screenWidth/2 - MeasureText("CREDITS", tamanosubtitulo - 4)/2, posBotonCredits.y + 12, tamanosubtitulo - 4, RED);
                        }
                        if (StateBotonCredits == 1){
                            DrawTexturePro(bluesheet, sourceBoton, posBotonCredits, origin, 0, WHITE);
                            DrawText("CREDITS", screenWidth/2 - MeasureText("CREDITS",tamanosubtitulo - 4)/2, posBotonCredits.y + 12, tamanosubtitulo - 4, WHITE);
                        }
                        else if (StateBotonCredits == 2){  //POSAR AIXO EN DRAWING
                            DrawTexturePro(bluesheet, sourceBotonpressed, posBotonpressedCredits, origin, 0, WHITE);
                            DrawText("CREDITS", screenWidth/2 - MeasureText("CREDITS",tamanosubtitulo - 4)/2, posBotonpressedCredits.y + 12, tamanosubtitulo - 4, WHITE);
                        }
                        
                    }
                    
                    
                    
                    

                    
                    if (bugmode)
                    {
                        DrawText(FormatText("seconds: %1.1f", seconds), screenWidth - 200, 20, 20, textcolor);
                        DrawText(FormatText("state: %d", state), screenWidth - 200, 40, 20, textcolor);
                        DrawText(FormatText("alpha: %1.1f", alpha), screenWidth - 200, 60, 20, textcolor);
                        DrawText(FormatText("framesCounterTitle: %d", framesCounterTitle), screenWidth - 400, 80, 20, textcolor);
                        //DrawText(FormatText("velocidad: %1.1f", (screenWidth/2 - longitudtitulo.x)/postitle.x), screenWidth - 200, 40, 20, BLACK);
                    }
                    
                    longitudtitulo = MeasureTextEx(tituloTtf, titulo, tamanotitulo, 5);
                } break;
                
                
                //----------------------------------------------------------------------------------
                case OPTIONS:
                {
                    // Dibujar titulo opcions con subrayado
                    subtitulo = "OPTIONS";
                    longitudtitulo = MeasureTextEx (tituloTtf, subtitulo, tamanotitulo - 30, 5);
                    DrawTextEx(tituloTtf, subtitulo, (Vector2){screenWidth/2 - longitudtitulo.x/2, 50}, tamanotitulo - 30, 5, RED);
                    underlineoptions.x = screenWidth/2 - longitudtitulo.x/2;
                    underlineoptions.width = longitudtitulo.x;
                    DrawRectangleRec (underlineoptions, RED);
                    
                    tamanosubtitulo = 26;
                    DrawText("Game Mode", screenWidth/2 - 200, posBotonWhitebox.y, tamanosubtitulo, textcolor);
                    DrawTexturePro(bluesheet, sourceSliderleft, posBotonSliderleft, origin, 0, WHITE);
                    DrawTexturePro(bluesheet, sourceSliderright, posBotonSliderright, origin, 0, WHITE);
                    DrawTexturePro(whitebox, sourceWhitebox, posBotonWhitebox, origin, 0, WHITE);
                    if (singleplayer)
                    {
                        DrawTexturePro(bluesheet, sourceSliderleft, posBotonSliderleft1, origin, 0, WHITE);
                        DrawTexturePro(bluesheet, sourceSliderright, posBotonSliderright1, origin, 0, WHITE);
                        DrawTexturePro(whitebox, sourceWhitebox, posBotonWhitebox1, origin, 0, WHITE);
                        posBotonCheck.y = posBotonWhitebox1.y + 50;
                    }
                    else
                    {
                        posBotonCheck.y = posBotonWhitebox1.y;
                    }
                    
                    
                    if (darkbackground){
                        DrawTexturePro(checkmark, sourceCheck, posBotonCheck, origin, 0, WHITE);
                    }
                    else{
                        DrawTexturePro(cross, sourceCheck, posBotonCheck, origin, 0, WHITE);
                    }
                    
                    
                    if (singleplayer){
                        DrawText("1 PLAYER", posBotonWhitebox.x + 85 - MeasureText("1 PLAYER",tamanosubtitulo - 5)/2, posBotonWhitebox.y + 7, tamanosubtitulo - 5, BLACK);
                        DrawText("Difficulty", screenWidth/2 - 200, posBotonWhitebox1.y, tamanosubtitulo, textcolor);
                        if (difficultycounter == 0){
                            DrawText("EASY", posBotonWhitebox1.x + 85 - MeasureText("EASY",tamanosubtitulo - 5)/2, posBotonWhitebox1.y + 7, tamanosubtitulo - 5, BLACK);
                        }
                        if (difficultycounter == 1){
                            DrawText("NORMAL", posBotonWhitebox1.x + 85 - MeasureText("NORMAL",tamanosubtitulo - 5)/2, posBotonWhitebox1.y + 7, tamanosubtitulo - 5, BLACK);
                        }
                        if (difficultycounter == 2){
                            DrawText("DIFFICULT", posBotonWhitebox1.x + 85 - MeasureText("DIFFICULT",tamanosubtitulo - 5)/2, posBotonWhitebox1.y + 7, tamanosubtitulo - 5, BLACK);
                        }
                        if (difficultycounter == 3){
                            DrawText("IMPOSSIBLE", posBotonWhitebox1.x + 85 - MeasureText("IMPOSSIBLE",tamanosubtitulo - 5)/2, posBotonWhitebox1.y + 7, tamanosubtitulo - 5, BLACK);
                        }
                    }
                    else
                    {
                        DrawText("2 PLAYER", posBotonWhitebox.x + 85 - MeasureText("2 PLAYER",tamanosubtitulo - 5)/2, posBotonWhitebox.y + 7, tamanosubtitulo - 5, BLACK);
                    }
                    
                    DrawText("Dark Background", screenWidth/2 - 200, posBotonCheck.y, tamanosubtitulo, textcolor);
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    //-----------------------------------------------------
                        if (StateBotonCredits == 0){
                            DrawTexturePro(bluesheet, sourceBoton, posBotonCredits, origin, 0, WHITE);
                            DrawText("BLACK TO", screenWidth/2 - MeasureText("BLACK TO", tamanosubtitulo - 7)/2, posBotonCredits.y + 6, tamanosubtitulo - 7, RED);
                            DrawText("MENU", screenWidth/2 - MeasureText("MENU", tamanosubtitulo - 7)/2, posBotonCredits.y + 22, tamanosubtitulo - 7, RED);
                        }
                        if (StateBotonCredits == 1){
                            DrawTexturePro(bluesheet, sourceBoton, posBotonCredits, origin, 0, WHITE);
                            DrawText("BLACK TO", screenWidth/2 - MeasureText("BLACK TO", tamanosubtitulo - 7)/2, posBotonCredits.y + 6, tamanosubtitulo - 7, WHITE);
                            DrawText("MENU", screenWidth/2 - MeasureText("MENU", tamanosubtitulo - 7)/2, posBotonCredits.y + 22, tamanosubtitulo - 7, WHITE);
                        }
                        else if (StateBotonCredits == 2){  //POSAR AIXO EN DRAWING
                            DrawTexturePro(bluesheet, sourceBotonpressed, posBotonpressedCredits, origin, 0, WHITE);
                            DrawText("BLACK TO", screenWidth/2 - MeasureText("BLACK TO", tamanosubtitulo - 7)/2, posBotonpressedCredits.y + 6, tamanosubtitulo - 7, WHITE);
                            DrawText("MENU", screenWidth/2 - MeasureText("MENU", tamanosubtitulo - 7)/2, posBotonpressedCredits.y + 22, tamanosubtitulo - 7, WHITE);
                        }
                    
                    
                }break;
                
                
                //----------------------------------------------------------------------------------
                case GAMEPLAY:
                {
                    // Draw GAMEPLAY screen here!
                    // Dibujar campo
                    DrawRectangleRec (techo, colorCampo);
                    DrawRectangleRec (suelo, colorCampo);
                    DrawRing(poscirculo, 80, 90, 0, 365, 30, colorCampo);
                    DrawCircleV(poscirculo, ballRadius, colorCampo);  
                        // Dibujar linea discontinua vertical al medio de la pantalla
                    for(discontinuas.y = techo.height + discontinuas.height/2; discontinuas.y < screenHeight - suelo.height; discontinuas.y += discontinuas.height * 1.5)  
                        {
                        DrawRectangleRec(discontinuas, colorCampo);
                        }
                    
                    DrawCircleV(ballPosition, ballRadius, BLUE);
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    
                    DrawRectangleRec (player, RED);
                    DrawRectangleRec (enemy, RED);
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    DrawRectangleRec (backRect1, BLACK);
                    DrawRectangleRec (backRect2, BLACK);
                    DrawRectangleRec (fillRect1, RED);
                    DrawRectangleRec (fillRect2, RED);
                    DrawRectangleRec (lifeRect1, YELLOW);
                    DrawRectangleRec (lifeRect2, YELLOW);



                    // TODO: Draw time counter.......................(0.5p)
                    tamanocontador = MeasureText(FormatText("%d",secondsCounter), 35);
                    DrawText(FormatText("%d", secondsCounter), screenWidth/2 - tamanocontador/2, 6, 35, BLACK);
                    
                    // TODO: Draw pause message when required........(0.5p)
                    if (pause)
                    {
                        DrawRectangle(0, 0, screenWidth, screenHeight, Fade(textcolor, 0.7f));
                        DrawText("PAUSE", screenWidth/2 - MeasureText("PAUSE",40)/2, screenHeight/2 -20, 40, RED);
                        DrawText("Press P again to Resume", screenWidth/2 - MeasureText("Press P again to Resume",20)/2, screenHeight/2 + 20, 20, RED);
                    }
                    
                                        
                    if (pausescore)//Si pasusescore true 
                    {
                        DrawText("PRESS SPACE TO START", screenWidth/2 - MeasureText("PRESS SPACE TO START",30)/2, screenHeight/2 - 13, 30, RED);
                    }
                    
                    
                    if (bugmode)
                    {
                        //DrawLine (iaLinex, 0, iaLinex, screenHeight, RED);    //SE BUGEA Y NO SE PORQUE, ES DONDE PONE SCREENHEIGHT PERO NO LO ENTIENDO
                        DrawText(FormatText("PlayerLife: %d", playerLife), screenWidth - 200, 20, 20, textcolor);
                        DrawText(FormatText("EnemyLife: %d", enemyLife), screenWidth - 200, 40, 20, textcolor);
                        DrawText(FormatText("collisionB.P.: %d", collisionballplayer), screenWidth - 200, 60, 20, textcolor);
                        DrawText(FormatText("Ball Speed: %0.1f", ballSpeed.x), screenWidth - 200, 80, 20, textcolor);
                    }
                    
                } break;
                
                
                 case CREDITS:
                {
                    DrawRectangle (0, 0, screenWidth, screenHeight, Fade(textcolor, 0.1f));
                    if (darkbackground)
                    {
                        textcolor = LIGHTGRAY;
                    }
                    else{
                        textcolor = DARKGRAY;
                    }
                    DrawText("Game made by: Eric Valls", screenWidth/2 - MeasureText("Game made by: Eric Valls",40)/2, 50, 40, RED);
                    DrawText("Realiced the 1st of December 2019", screenWidth/2 - MeasureText("Realiced the 1st of December 2019",25)/2, 100, 25, WHITE);
                    DrawText("This game was made as a schoolwork using the C language of programming\nand improved by my own to make it presentable and with a better game play.", screenWidth/2 - MeasureText("This game was made as a schoolwork using the C language of programming",18)/2, 180, 18, textcolor);
                    DrawText("I spent a lot of hours in this project so...", screenWidth/2 - MeasureText("I spent a lot of hours in this project so...",18)/2, 280, 18, textcolor);
                    DrawText("I hope you like it! :)", screenWidth/2 - MeasureText("I hope you like it! :)",26)/2, 300, 26, textcolor);
                    
                    if (StateBotonCredits == 0){
                        DrawTexturePro(bluesheet, sourceBoton, posBotonCredits, origin, 0, WHITE);
                        DrawText("BLACK TO", screenWidth/2 - MeasureText("BLACK TO", tamanosubtitulo - 7)/2, posBotonCredits.y + 6, tamanosubtitulo - 7, RED);
                        DrawText("MENU", screenWidth/2 - MeasureText("MENU", tamanosubtitulo - 7)/2, posBotonCredits.y + 22, tamanosubtitulo - 7, RED);
                    }
                    if (StateBotonCredits == 1){
                        DrawTexturePro(bluesheet, sourceBoton, posBotonCredits, origin, 0, WHITE);
                        DrawText("BLACK TO", screenWidth/2 - MeasureText("BLACK TO", tamanosubtitulo - 7)/2, posBotonCredits.y + 6, tamanosubtitulo - 7, WHITE);
                        DrawText("MENU", screenWidth/2 - MeasureText("MENU", tamanosubtitulo - 7)/2, posBotonCredits.y + 22, tamanosubtitulo - 7, WHITE);
                    }
                    else if (StateBotonCredits == 2){  //POSAR AIXO EN DRAWING
                        DrawTexturePro(bluesheet, sourceBotonpressed, posBotonpressedCredits, origin, 0, WHITE);
                        DrawText("BLACK TO", screenWidth/2 - MeasureText("BLACK TO", tamanosubtitulo - 7)/2, posBotonpressedCredits.y + 6, tamanosubtitulo - 7, WHITE);
                        DrawText("MENU", screenWidth/2 - MeasureText("MENU", tamanosubtitulo - 7)/2, posBotonpressedCredits.y + 22, tamanosubtitulo - 7, WHITE);
                    }
                    
                } break;
                
                case ENDING: 
                {
                    // Draw END screen here!
                    DrawRectangle(0, 0, screenWidth, screenHeight, Fade(textcolor, 0.1f));
                    DrawRectangle(40, 40, screenWidth - 80, screenHeight - 80, Fade(colorresultado, 0.7f));
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    if (singleplayer)
                    {
                        DrawText(result, screenWidth/2 - MeasureText(result,30)/2, screenHeight/2 - 60, 30, textcolor);
                        DrawText(result2linea, screenWidth/2 - MeasureText(result2linea,20)/2, screenHeight/2 - 20, 20, textcolor);
                    }
                    else{
                        DrawText(result, screenWidth/2 - MeasureText(result,50)/2, screenHeight/2 - 60, 50, textcolor);
                    }
                    DrawText("Pres ENTER to back to menu", screenWidth/2 - MeasureText("Pres ENTER to back to menu",20)/2, screenHeight/2 + 50, 20, backgroundcolor);
                    DrawText("Pres ESC to Exit", screenWidth/2 - MeasureText("Pres ESC to Exit",20)/2, screenHeight/2 + 80, 20, backgroundcolor);
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    UnloadFont(tituloTtf);
    
    UnloadTexture(bluesheet);  // Unload button texture
    UnloadTexture(whitebox);
    UnloadTexture (checkmark);
    UnloadTexture (cross);
    

    UnloadSound (choque);
    UnloadSound (incrementovel);
    UnloadSound (goal);
    UnloadSound (pausar);
    UnloadSound (continuar);
    UnloadSound (ganar);
    UnloadSound (boton);
    CloseAudioDevice();
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}